# Delivery Avenida

# Disciplina de Algoritmos e Programação III

Repositório criado para o trabalho da disciplina de algoritmos e programação III.

## 1. Das diretrizes gerais de desenvolvimento

- Página Web criada para a comercialização de produtos alimentícios, através da montagem de seu prato preferido.


## 2. Do desenvolvimento e implementação

- Front-end será desenvolvido com tecnologia React

- Back-end será desenvolvido na linaguagem Java utilizando framework Spring Boot.

- Banco de dados será com a tecnologia MySQL.

- Poderão ser implementadas outras tecnologias no decorer do desenvolvimento da aplicação.

## 3. Da equipe

- A equipe é composta pelos integrantes Gabriel Menezes e Pedro, ambos responsáveis pelo front-end e Patrick Gonçalves e Rodrigo Pedrotti, ambos responsáveis pelo back-end.

## 4. Diagrama ER do banco de dados

